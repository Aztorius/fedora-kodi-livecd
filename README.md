# Fedora-Kodi livecd
A live CD creator script to build a Fedora version with Kodi preinstalled.

## Build ISO image (ROOT only)

```sh
./fedora-kodi-livecd.sh
```

## Install Fedora-Kodi

Currently you can only use the ISO image on a CD/DVD or USB stick and use it.
Installation of Fedora-Kodi is not yet available.
