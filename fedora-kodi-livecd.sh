#!/bin/bash

###############################################################################
# This is a live cd creator script for fedora custom with kodi
# Check the GitHub repository : https://github.com/Aztorius/fedora-kodi-livecd
###############################################################################

echo "Install required dependencies (ROOT)"
sudo dnf install livecd-tools spin-kickstarts genisoimage

echo "Create custom ISO image (ROOT)"
sudo livecd-creator --verbose \
       --fslabel=Fedora-Kodi \
       --releasever=27 \
       --config=fedora-kodi.ks
