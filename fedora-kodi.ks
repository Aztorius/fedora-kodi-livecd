###################################
# Created by William Bonnaventure #
# GPLv2 Licence                   #
###################################

# Include fedora-base-live
%include fedora-live-base.ks

# Add RPMFusion repository
repo --name=rpmfusion-free --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os
repo --name=rpmfusion-free-updates --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch

# Install custom packages
%packages

# RPMFusion release package
rpmfusion-free-release

# Specific X11 related packages
dbus-x11
upower

# Network files shares
samba-client

# Graphics drivers and libraries
mesa-omx-drivers
mesa-libGLES
mesa-vdpau-drivers
mesa-libglapi
libvdpau
vdpauinfo
# Intel specific hardware decoding
libva-intel-driver
libva-utils
libvdpau-va-gl

# DVD playback
libdvdnav
libdvdread
# libdvdcss : missing due to legal issues

# Bluray playback
libbluray
libbluray-bdj
libaacs
libbdplus

# Kodi (no DVD/BD support with RPMFusion default build)
kodi
kodi-eventclients
kodi-platform

%end

# Reboot after installation
reboot

%post

###################################
# Add custom systemd service file #
###################################
cat <<"ENDcat" >/etc/systemd/system/kodi.service
[Unit]
Description = kodi-standalone using xinit
After = remote-fs.target systemd-user-sessions.service

[Service]
User = liveuser
Group = liveuser
Type = simple
ExecStart = /usr/bin/xinit /usr/bin/dbus-launch /usr/bin/kodi-standalone -- :0 -nolisten tcp
Restart = on-abort

[Install]
WantedBy = multi-user.target
ENDcat

#############################################
# Allow Kodi to use power related functions #
#############################################

cat <<"ENDcat" >/etc/polkit-1/localauthority/50-local.d/kodi_shutdown.pkla
[Actions for kodi user]
Identity=unix-user:liveuser
Action=org.freedesktop.devicekit.power.*;org.freedesktop.upower.*;org.freedesktop.consolekit.system.*;org.freedesktop.login1.*
ResultAny=yes
ENDcat

########################
# Allow X server start #
########################

echo "allowed_users = anybody" > /etc/X11/Xwrapper.config

systemctl daemon-reload
systemctl enable kodi.service

%end
